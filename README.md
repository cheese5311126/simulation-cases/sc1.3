# Physics-based PSHA and urgent UQ for Iceland: next generation hazard map

**SC leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/LMU.png?inline=false" width="100">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/IMO.png?inline=false" width="100">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/bsc.png?inline=false" width="200">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/TUM.png?inline=false" width="100">

**Codes:** [SeisSol](https://gitlab.com/cheese5311126/codes/seissol), CyberShake, [ExaHyPE](https://gitlab.com/cheese5311126/codes/exahype)

**Target EuroHPC Architectures:** MN5, Leonardo

**Type:** Hazard assessment, urgent computing

## Description

The main purpose of this simulation case is two-fold (1) to realise dynamic
earthquake source inversion for physics-based UQ and (2) to realise
physics-based dynamic and kinematic fault-rupture and ground motion
simulations of realistic earthquake scenarios, such that physics-based seismic
ground-motion predictions can be helpful for urgent computing and seismic
hazard applications. This SC will use the ChEESE-2P flagship codes SeisSol and
ExaHyPE, for dynamic and kinematic fault-rupture and ground-motion
simulations, and CyberShake, a code for physics-based probabilistic seismic
hazard assessment.

In part 1 of this SC, we apply a newly established earthquake dynamic source
inversion workflow to the 2004 Parkfield, California, earthquake. To this end, we
successfully prototyped the workflow with a simplified finite-difference forward
solver and now aim to demonstrate its feasibility with SeisSol. Dynamic source
inversion provides a physically consistent and accurate heterogeneous source
model and uncertainty quantification. We will also explore additional complexity
in earthquake simulations to better capture the complexity of ground shaking,
including topography, rough fault, and fault zone models comparing ExaHyPE
and SeisSol simulations. Our results may aid seismic hazard assessment by
constraining ground motion variability due to source complexity and identifying
particularly destructive rupture phases.

In part 2, we intend to apply and enhance the CyberShake workflow by
exploiting SeisSol characteristics for the simulation of more complex physics
models in fault-rupture (e.g., perturbation in rupture parameters, fault
roughness) and wave propagation (e.g., through media with a high-velocity
contrast and including high-resolution topography). Our study region is
Southwest Iceland. The seismic cycle model for Southern Iceland from
Simulation Case 1.2 will provide an Earthquake Rupture Forecast for CyberShake
calculations.

In part 3, we use Cybershake to populate a new synthetic database for Southern
California and train ML-based models from said database. The methodology,
named MLESmap (Machine Learning based Estimator for ground Shaking maps),
has already been applied to the Los Angeles basin region (Monterrubio-Velasco
et al., 2024) using physics-based simulations from the CyberShake 15.4 study
(Graves et al, 2011; Jordan and Callaghan, 2018). The resulting models provide
almost instant outcomes in terms of PSA for the studied region, and hence are
candidates to complement urgent computing runs. Besides urgent computing,
this approach can also be used for parametric exploration (i.e. UQ) and early risk
assessment.

<img src="SC1.3.3.png" width="">

**Figure 3.3.2.** MLESmap workflow that sequentially resolves the necessary steps towards
producing a ML surrogate of a physics-based synthetic earthquake hazard database. The
offline phase includes four main steps, (1) generation of synthetic scenarios simulated
with the CyberShake platform, (2) data preparation and data analysis, (3) ML training
and modeling and (4) model evaluation.

Lastly, we will explore hybrid machine learning and HPC-based strategies for
rapidly computing shake maps from existent dynamic rupture scenarios and
Cybershake scenarios.

<img src="SC1.3.png" width="460"><img src="SC1.3.1.png" width="500"><img src="SC1.3.2.png" width="400">

**Figure 3.3.1.** Above: Complex joint dynamic rupture model of the 2019 Ridgecrest
earthquake and the Searles Valley foreshock. The figure depicts the complex orthogonal
fault system with small-scale fault roughness, the 3D velocity structure including a
shallow fault damage zone surrounding the fault system, and the high-resolution
topography. The model uses a fast-velocity weakening rate-and-state friction law and
incorporates viscoelastic attenuation and off-fault plasticity. The shown ground velocity is
caused by the Ridgecrest mainshock.

**Below:** Ground-motion simulations for Southwestern Iceland with CyberShake by Rojas
et al. (2024). (left) The map shows the simulation domain. Surface projections of 223
strike-slip fault sources are represented with red lines. Main population centers are
represented with circles, and locations of hypothetical recording stations are represented
with cyan triangles. (right) Synthetic ground motion (black circles) is compared with the
prediction of empirical models (color lines) and observations of the few recorded events
(red circles).


## Expected results

Enable the open-source, multi-physics software SeisSol as a plugin within CyberShake, thus adding
versatility to the platform. A Seissol-enabled CyberShake model can introduce novel features such as topography or thin
sedimentary basins to Iceland’s PSHA models, while benefiting from future developments in SeisSol both within
ChEESE-2P and elsewhere. This novel PSHA platform will then enable hazard studies at multiple space-time scales that help
bridging fundamental issues with existing PSHA models (e.g. recurrence models, wave physics). This SC will also utilize the
MUQ capabilities of ExaHyPE and SeisSol in a rapid response setting and use ML/AI models, trained from HPC simulation
results and observations, able to reproduce with substantial accuracy the outcomes of full-field HPC simulations.